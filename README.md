# Airbnb-newyork

Trabajo práctico 2 para la asignatura de Tipología y ciclo de vida de los datos, Máster en Ciencia de datos

# Equipo
Fabio Durán Verdugo y Daniel Oriol Asensio. 

# Descripción de los ficheros
* source/Airbnb-newyork.Rmd: Fichero markdown con el desarrollo de la actividad, incluye códigos en R.
* docs/airbnb-newyork.html: Fichero html con el output del documento generado para el desarrollo de la actividad
* docs/airbnb-newyork.pdf: fichero pdf con el output del documento generado para el desarrollo de la actividad
* dataset/AB_NYC_2019.csv: Dataset original
* dataset/Airbnb-newyork-clean.csv: Fichero csv con el dataset tratado o limpio
